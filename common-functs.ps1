
function curr_dir() { 
  return Split-Path $MyInvocation.ScriptName  
}

function rm_rf($path) {
  if (Test-Path $path) {
    Remove-Item -Path $path -Recurse -Force
  }
}

function sln_dir() {
  $cd = curr_dir
  $dir = $cd
  
  while ((Get-ChildItem -Filter "*.sln" -Path $dir) -eq $null) {
    if  ( ($dir -eq $null) -or ($dir.length -eq 0) ) { 
      return $dir;
    }
    
    $dir = Split-Path $dir;
  } 
  
  return $dir
}

function rel_path_dir() {
  $cd = curr_dir
  $dir = $cd
  $parts = @()
  
  while ((Get-ChildItem -Filter "*.sln" -Path $dir) -eq $null) {
    if  ( ($dir -eq $null) -or ($dir.length -eq 0) ) { 
      return ($parts -join "\");
    }
    
    $parts += Split-Path -Leaf -Path $dir
    
    $dir = Split-Path $dir;
  } 
  
  return ($parts -join "\")
}
