To Use this

do the following:

cd /path/to/your/project
git submodule add -f https://bitbucket.org/base2art/winfx-build-scripts.git <scripts>


project requirements and structure:


build/
      <project-1>.nuspec
      <project-2>.nuspec
      ...


src/
    common/
           ProductInfo.cs
           CompanyInfo.cs

    <project-1>/
                bin/
                    Release/
                    Debug/

    <project-2>/
                bin/
                    Release/
                    Debug/

    ...

test/

     <project-1>/
                 bin/
                     Release/
                     Debug/

     <project-2>/
                 bin/
                     Release/
                     Debug/

     ...


