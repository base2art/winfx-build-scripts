
function curr_dir() { return Split-Path $MyInvocation.ScriptName  }
$cd = curr_dir
. "$cd\common-functs.ps1"

$sdir = sln_dir
$rpdir = rel_path_dir


function rev_file_version($path) {

  if (Test-Path $path) {
    $c = Get-Content $path
    $matches = [System.Text.RegularExpressions.Regex]::Matches($c, """(\d+\.\d+\.\d+(?:\.\d+)?)""")
    foreach ($match in $matches) {
      $match = $match.Groups[1]
      $v = [System.Version]::Parse($match)
      if ($v.Revision -ne 0) {
        $v = New-Object System.Version($v.Major, $v.Minor, $v.Build, ($v.Revision + 1));
      } elseif ($v.Build -ne 0) {
        $v = New-Object System.Version($v.Major, $v.Minor, ($v.Build + 1), $v.Revision);
      } elseif ($v.Minor -ne 0) {
        $v = New-Object System.Version($v.Major, ($v.Minor + 1), $v.Build, $v.Revision);
      } elseif ($v.Major -ne 0) {
        $v = New-Object System.Version(($v.Major + 1), $v.Minor, $v.Build, $v.Revision);
      } else {
        $v = New-Object System.Version($v.Major, $v.Minor, ($v.Build + 1), $v.Revision);
      }
      
      $c = $c.Replace($match, $v.ToString())
    }
    
    Set-Content -Value $c -Path $path
  }
}

rev_file_version "$sdir\src\common\ProductInfo.cs"

$folders = Get-ChildItem -Exclude "common" -Path "$sdir\src"
foreach ($folder in $folders) {
  rev_file_version "$folder\Properties\AssemblyInfo.cs"
}

