
function curr_dir() { return Split-Path $MyInvocation.ScriptName  }
$cd = curr_dir
. "$cd\common-functs.ps1"

$baseDir = sln_dir
$outputFolder = Join-Path $baseDir "Output"

$msbuild = "c:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe"
if (-Not (Test-Path $msbuild)) {
  $msbuild = "c:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe"
}

$options = "/consoleloggerparameters:ErrorsOnly /nologo /verbosity:quiet /p:Configuration=Release"


if (Test-Path $outputFolder) {
 rmdir -Recurse $outputFolder
}

$wd = pwd
cd $baseDir

$slnPath = (Get-ChildItem -Filter "*.sln" -Path $baseDir)[0]

$sln = Split-Path -Leaf -Path $slnPath

$clean = $msbuild + " ""$sln"" " + $options + " /t:Clean"
$build = $msbuild + " ""$sln"" " + $options + " /t:Build"
Invoke-Expression $clean
Invoke-Expression $build

$sallow = mkdir $outputFolder

$sdir = sln_dir

$folders = Get-ChildItem -Exclude "common" -Path "$sdir\src"
foreach ($folder in $folders) {
  if (Test-Path $folder) {
    cp -Recurse "$folder\bin\Release\*.exe" $outputFolder
    cp -Recurse "$folder\bin\Release\*.dll" $outputFolder
    cp -Recurse "$folder\bin\Release\*.pdf" $outputFolder
  }
}

cd $wd
