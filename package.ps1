
function curr_dir() { return Split-Path $MyInvocation.ScriptName  }
$cd = curr_dir
. "$cd\common-functs.ps1"

$baseDir = sln_dir


function delete_packing($create) {
  if (Test-Path -path "$baseDir\packing") {
    Remove-Item -Recurse -Force "$baseDir\packing"
  }
  
  if ($create) {
    mkdir "$baseDir\packing"
  }
}

function Get-PackageDatas() {
  
  $nuspecs = Get-ChildItem -Filter "*.nuspec" -Path "$baseDir\build"
  
  forEach ($nuspec in $nuspecs) {
    echo "3"
    $path = Split-Path -Leaf -Path $nuspec;
    $filePath = "$baseDir\build\$path"
    
    cp $filePath "$baseDir\packing"
    
    [xml]$pkg = Get-Content $filePath
    $projName = $pkg.package.metadata.id
    $file_src_full = $null
    $pkg.package.files.file | ForEach-Object {
      $file_src1 = $_.src.ToString();
      $isDll = $file_src1.EndsWith(".dll") -or  $file_src1.EndsWith(".exe")
      if ($isDll) {
        if ( -not ($file_src_full) ) {
          $file_src_full = "$baseDir\src\$projName\bin\Release\$file_src1"
        } 
      }
      cp "$baseDir\src\$projName\bin\Release\$file_src1" "$baseDir\packing\" 
    }
    
    $version = [System.Diagnostics.FileVersionInfo]::GetVersionInfo($file_src_full).FileVersion.ToString()
    
    $nuspecPath = "$baseDir\packing\$path"
    (Get-Content $nuspecPath) | 
      Foreach-Object {$_ -replace "AAAA.BBBB.CCCC.DDDD", $version} | 
      Set-Content $nuspecPath
    
    $expr = "$baseDir\packages\NuGet.CommandLine.2.8.2\tools\NuGet.exe pack ""$nuspecPath""  -OutputDirectory ""$baseDir\packing\"" "
    Invoke-Expression $expr
    
    cp "$baseDir\packing\*.nupkg" $outputDirectory
  }
}

$outputDirectory = "$HOME\NugetServer\Packages"

if (-Not (Test-Path $outputDirectory)) {
    mkdir $outputDirectory
}

delete_packing $true

Get-PackageDatas

delete_packing $false

echo "Done..."




#process_package $outputDirectory ".\Base2art.BCL.nuspec" @(
#			"..\src\Base2art.BCL\bin\Release\Base2art.BCL.dll",
#			"..\src\Base2art.BCL\bin\Release\Base2art.BCL.pdb"
#			)
#
#Write-Host "Press any key to continue ..."
#$x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
#
#
#
#
# $cwd = pwd	
# cd $cwd
#
#
#function copy_to($paths) {
#  foreach ($item in $paths) {
#    cp $item "$baseDir\packing"
#  }
#}
#
#
#
#function process_package($dest, $nuspecName, $paths) {
#  delete_packing $true
#  
#  cd "$baseDir\packing"
#  $wd = pwd
#  echo "Working Dir: $wd"
#  $firstPath = Join-Path $wd $paths[0]
#  $val = [System.Diagnostics.FileVersionInfo]::GetVersionInfo($firstPath)
#  
#  $version = $val.FileVersion.ToString()
#  $paths += "..\scripts\pack\$nuspecName"
#  copy_to $paths
#			
#  (Get-Content ..\packing\*.nuspec) | 
#    Foreach-Object {$_ -replace "AAAA.BBBB.CCCC.DDDD", $version} | 
#    Set-Content ..\packing\*.nuspec
#  
#  ..\packages\NuGet.CommandLine.2.8.2\tools\NuGet.exe pack $nuspecName
#  cd ..\scripts\pack
#  
#  cp ..\..\packing\*.nupkg $dest
#  
#  #delete_packing $false
#}










#process_package ".\Base2art.MockUI.nuspec" @(
#			"..\src\Base2art.MockUI\bin\Release\Base2art.MockUI.dll",
#			"..\src\Base2art.MockUI\bin\Release\Base2art.MockUI.pdb",
#			"..\src\Base2art.MockUI.CsQuery\bin\Release\Base2art.MockUI.CsQuery.dll",
#			"..\src\Base2art.MockUI.CsQuery\bin\Release\Base2art.MockUI.CsQuery.pdb"
#			)
#
#process_package ".\Base2art.WebRunner.nuspec" @(
#			"..\src\Base2art.WebRunner\bin\Release\Base2art.WebRunner.dll",
#			"..\src\Base2art.WebRunner\bin\Release\Base2art.WebRunner.pdb",
#			"..\src\Base2art.WebRunner.CsQuery\bin\Release\Base2art.WebRunner.CsQuery.dll",
#			"..\src\Base2art.WebRunner.CsQuery\bin\Release\Base2art.WebRunner.CsQuery.pdb"
#			)

