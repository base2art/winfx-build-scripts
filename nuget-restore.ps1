
function curr_dir() { return Split-Path $MyInvocation.ScriptName  }
$cd = curr_dir


function download-nuget() {
  $nugetWeb = "http://www.nuget.org/api/v2/package/Nuget.CommandLine"
  $tempDir = [System.IO.Path]::GetTempPath()
  $tempFile = Join-Path $tempDir "nuget.zip"
  $nugetDir = Join-Path $tempDir "nuget"
  if (-Not (Test-Path $tempFile)) {
    $webclient = New-Object System.Net.WebClient
    $webclient.DownloadFile($nugetWeb, $tempFile)
  }
  
  $nugetDest = Join-Path $nugetDir 'tools\nuget.exe'
  if (-Not (Test-Path $nugetDest)) {
    [System.Reflection.Assembly]::LoadWithPartialName('System.IO.Compression.FileSystem')
    if (Test-Path $nugetDir) {
      rmdir -Recurse $nugetDir
    }
    
    mkdir $nugetDir
    [System.IO.Compression.ZipFile]::ExtractToDirectory($tempFile, $nugetDir)
  }
  
  return $nugetDest
}


$target = download-nuget

$rootDir = "$cd\.."

$slns = Get-ChildItem -Path $rootDir -Filter "*.sln"

cd $rootDir

foreach($sln in $slns) {
  $slnName = Split-Path -Leaf $sln
  # echo (Get-Location)
  # echo $slnName
  & $target "restore" "$slnName"
}


