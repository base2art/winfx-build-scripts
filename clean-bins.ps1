

function curr_dir() { return Split-Path $MyInvocation.ScriptName  }
$cd = curr_dir
. "$cd\common-functs.ps1"

$baseDir = sln_dir

$srcDir = Join-Path $baseDir "src"
$testDir = Join-Path $baseDir "test"



function cleanBy($path) {
  ForEach ($folder in (Get-ChildItem -Path $path)) {
    rm_rf (Join-Path $folder.FullName "bin")
    rm_rf (Join-Path $folder.FullName "obj")
    rm_rf (Join-Path $folder.FullName "OpenCover")
  }
}

rm_rf (Join-Path $baseDir "Output")
rm_rf (Join-Path $baseDir "packing")

cleanBy $srcDir
cleanBy $testDir


